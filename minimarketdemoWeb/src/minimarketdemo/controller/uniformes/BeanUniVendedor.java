package minimarketdemo.controller.uniformes;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.PryProyecto;
import minimarketdemo.model.core.entities.UniCliente;
import minimarketdemo.model.core.entities.UniPedido;
import minimarketdemo.model.core.entities.UniProforma;
import minimarketdemo.model.uniformes.managers.ManagerUniformes;

@Named
@SessionScoped
public class BeanUniVendedor implements Serializable {

	@EJB
	private ManagerUniformes mUniformes;
	private List<UniProforma> listaProformas;
	private UniProforma nuevaProforma;
	private List<UniCliente> listaClientes;
	private List<UniPedido> listaUniformes;
	
	private String[] estado = {"abonado","pagado","entregado"};
	
	public BeanUniVendedor() {
		// TODO Auto-generated constructor stub
	}
	@PostConstruct
	public void inicializar() {
		listaProformas = mUniformes.findAllProformas();
		nuevaProforma = mUniformes.inicializarProforma();
	}
	
	
	//Metodos para proforma
	public String actionCargarProforma() {
		nuevaProforma = mUniformes.inicializarProforma();
		
		return "proforma";
	}
	
	public void actionListenerInsertarProforma() {
		try {
			mUniformes.insertarProforma(nuevaProforma);
			JSFUtil.crearMensajeINFO("Proforma Creada");
			listaProformas = mUniformes.findAllProformas();
			nuevaProforma = mUniformes.inicializarProforma();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	

	
	public void actionListenerActualizarEstado(UniProforma proforma) {
		try {
			mUniformes.actualizarEstado(proforma);
			JSFUtil.crearMensajeINFO("Estado actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<UniProforma> getListaProformas() {
		return listaProformas;
	}

	public void setListaProformas(List<UniProforma> listaProformas) {
		this.listaProformas = listaProformas;
	}

	public UniProforma getNuevaProforma() {
		return nuevaProforma;
	}

	public void setNuevaProforma(UniProforma nuevaProforma) {
		this.nuevaProforma = nuevaProforma;
	}

	public List<UniCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<UniCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<UniPedido> getListaUniformes() {
		return listaUniformes;
	}

	public void setListaUniformes(List<UniPedido> listaUniformes) {
		this.listaUniformes = listaUniformes;
	}

	public String[] getEstado() {
		return estado;
	}

	public void setEstado(String[] estado) {
		this.estado = estado;
	}
	
	//Metodos para cliente
	
	//Metodos para Uniforme

}
